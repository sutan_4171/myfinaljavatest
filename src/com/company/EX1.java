package com.company;

import java.util.Scanner;

public class EX1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int l = sc.nextInt();
        int num[] = new int[1000];
        int x = 0, a = 0, b = 0, c = 0, d = 0, e = 0;
        String n = new String();
        String s = new String();
        for (int i = 0; i <= l; i++) {
            n += sc.nextLine() + " ";
        }
        for (int i = 1; i < n.length(); i++) {
            s = n.substring(i, i + 1);
            if (n.charAt(i) == ' ') {
                x++;
                continue;
            } else {
                num[x] *= 10;
            }
            num[x] += Integer.parseInt(s);
        }
        for (int i = 0; i < x; i++) {
            if (num[i] >= 90) a++;
            else if (num[i] >= 80) b++;
            else if (num[i] >= 70) c++;
            else if (num[i] >= 60) d++;
            else e++;
        }
        System.out.println(">=90: " + a);
        System.out.println("80-89: " + b);
        System.out.println("70-79: " + c);
        System.out.println("60-69: " + d);
        System.out.println("<60: " + e);
    }
}
