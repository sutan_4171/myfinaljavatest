package com.company;

import java.util.Scanner;

public class EX2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int l=sc.nextInt();
        Scanner sc1 = new Scanner(System.in);
        char n[][]=new char[l][];
        String s[]=new String[l];
        int ans=0;
        for(int i=0;i<l;i++){
            s[i]=sc1.nextLine();
        }
        for(int i=0;i<l;i++){
            n[i]=s[i].toCharArray();
        }
        for(int i=0;i<l;i++){
            for(int j=0;j<n[i].length;j++){
                if(n[i][j]=='b'){
                    if(n[i-1][j]!='1') n[i-1][j]='x';
                    if(n[i+1][j]!='1') n[i+1][j]='x';
                    if(n[i][j-1]!='1') n[i][j-1]='x';
                    if(n[i][j+1]!='1') n[i][j+1]='x';
                }
                if(n[i][j]=='B'){
                    for(int k=i;k>0 && n[k][j]!='1' && k<l;k--){ n[k][j]='x';}
                    for(int k=i;k>0 && n[k][j]!='1' && k<l;k++){ n[k][j]='x';}
                    for(int k=j;k>0 && n[i][k]!='1' && k<n[i].length;k--){ n[i][k]='x';}
                    for(int k=j;k>0 && n[i][k]!='1' && k<n[i].length;k++){ n[i][k]='x';}
                }
            }
        }
        for(int i=0;i<l;i++) {
            for (int j = 0; j < n[i].length; j++) {
                //System.out.print(n[i][j]);
                if (n[i][j] == '0') ans++;
            }
            //System.out.println("");
        }
        System.out.println(ans);
    }
}
